const express = require('express');
const multer = require('multer');
const fs = require('fs');
const MongoClient = require('mongodb').MongoClient;
const xlsx_json = require('xlsx-to-json');

const mongoURL = (process.env.MONGODB_URI || 'mongodb://localhost:27017/cuems');
const app = express();
const request = require('superagent');

const upload = multer({dest: './uploads/'});
const exec = require("child_process").exec;

app.set('port', (process.env.PORT || 3001));

// Express only serves static assets in production
if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'));
}
MongoClient.connect(mongoURL).then(db => {
  function getGooglePlaceResult(name) {
    var params = {
      key: 'AIzaSyCm8ojOg3oKInbJX16GiJg_TX9Pn8DDWJs',
      location: '42.4469782,-76.4778971',
      radius: 10000,
      query: name
    }
    return request.get('https://maps.googleapis.com/maps/api/place/textsearch/json')
    .query(params)
    .set('Accept', 'application/json')
  }

  function updateAllPlaces() {
    var cursor = db.collection('calls')
      .find({})
      .toArray()
    .then((calls) => {
      var uniqueLocations = {};

      calls.forEach(call => {
        uniqueLocations[call.Location] = 1;
      });
      uniqueLocations = Object.keys(uniqueLocations);
      uniqueLocations.forEach(name => {
        getGooglePlaceResult(name).end((err, resp) => {
          if(err){
            console.log(err);
          }
          var place = JSON.parse(resp.text).results[0];
          if (place) {
            db.collection('calls').updateMany(
              { Location: name },
              { $set: {
                'googleLocation' : place.name,
                'lat': place.geometry.location.lat,
                'lng': place.geometry.location.lng
              } }
            );
          }
        });
      });
    });
  }

  app.post('/import', upload.single('upload'), (req, res) => {
    xlsx_json({
      input: __dirname + '/' + req.file.path,
      output: __dirname + '/' + req.file.path + '.json'
    }, function(err, result) {
      if(err) {
        console.error(err);
      } else {
        res.json(result);
        // insert result into DB
        db.collection('calls').remove({}).then(() => {
          var datesFixed = result.map(call => {
            call['Date'] = new Date(call['Date']);
            return call;
          });
          db.collection('calls').insertMany(datesFixed, function() {
            updateAllPlaces();
          });
        });
      }
    });
  });

  app.get('/populate', (req, res) => {
    var process = exec('python populate_db.py', {}, function (error, stdout, stderr) {
      updateAllPlaces();
      res.json({'status': 'done', 'error': error, 'stdout': stdout, 'stderr': stderr});
    });
  });

  app.get('/calls', (req, res) => {
    var query = {}

    if (req.query.date || req.query.startDate || req.query.endDate) {
      query['Date'] = {}
    }
    if (req.query.date) {
      query['Date'] = new Date(req.query.date);
    }
    if (req.query.startDate) {
      query['Date']['$gte'] = new Date(req.query.startDate);
    }
    if (req.query.endDate) {
      query['Date']['$lte'] = new Date(req.query.endDate);
    }

    var cursor = db.collection('calls')
      .find(query)
      .limit(100)
      .toArray()
    .then((data) => {
      res.json(data);
    });
  });

  app.listen(app.get('port'));

}).catch(err => {
  console.log(err);
});
