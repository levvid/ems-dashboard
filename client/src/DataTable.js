import React from 'react';
import {
  Table,
  TableHeader
} from 'react-mdl'

class DataTable extends React.Component {
  render() {

    return (
      <Table
          sortable
          shadow={0}
          rows={this.props.calls}
          className="TableView"
      >
        <TableHeader
          name="Run #"
          numeric
          tooltip="Run Number Unique ID"
        >
          Run Number
        </TableHeader>

        <TableHeader
          name="PCR #"
          sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
          tooltip="Run Number Unique ID"
        >
          PR ID
        </TableHeader>

        <TableHeader
          name="Date"
          numeric
          tooltip="Patient Report ID"
        >
          Date
        </TableHeader>

        <TableHeader
          name="CQI"
          sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
          tooltip="Continuous Quality Improvement"
        >
          Quality Improvement
        </TableHeader>

        <TableHeader
          name="Crew Chief"
           sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
           tooltip="Crew Chief for the incident"
          >
          Crew Chief
          </TableHeader>

          <TableHeader
            name="Crew"
            sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
            tooltip="Crew members present"
          >
          Crew
          </TableHeader>

          <TableHeader
            name="Driver"
            sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
            tooltip="Driver for the incident"
          >
          Driver
          </TableHeader>

          <TableHeader
            name="CCIT Eval Submitted?"
            sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
            tooltip="Crew Chief in Training Evaluation Submitted"
          >
          Evaluation
          </TableHeader>

           <TableHeader
            name="Location"
            sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
            tooltip="Location of incident"
          >
          Location
          </TableHeader>

           <TableHeader
           name="Dispatch Info"
           sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
           tooltip="Dispatch Information"
          >
          Dispatch
          </TableHeader>

           <TableHeader
           name="Chief Complaint"
           sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
           tooltip="Chief Complaint"
          >
          Complaint
          </TableHeader>

           <TableHeader
           name="Call Type"
           sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
           tooltip="The call type"
          >
          Call Type
          </TableHeader>

           <TableHeader
           name="Disposition"
           sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
           tooltip="The patient's disposition"
          >
          Disposition
          </TableHeader>

           <TableHeader
           name="RLS"
           sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
           tooltip="RLS"
          >
          RLS
          </TableHeader>

           <TableHeader
           name="Air Medical"
           sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
           tooltip="Whether the patient required air transport"
          >
          Air Medical
          </TableHeader>

           <TableHeader
           name="Equipment Used"
           sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
           tooltip="Equipment Used"
          >
          Equipment
          </TableHeader>

           <TableHeader
           name="Glucometer Used"
           sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
           tooltip="Whether or not the glucometer was used"
          >
          Glucometer
          </TableHeader>

           <TableHeader
           name="Key Card Used"
           sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
           tooltip="Whether or not a keycard was used"
          >
          Keycard
          </TableHeader>

           <TableHeader
           name="EtOH Related"
           sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
           tooltip="Whether or not the incident was ETOH related"
          >
          ETOH
          </TableHeader>

           <TableHeader
           name="Presenting Problem"
           sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
           tooltip="Whether or not there was a problem presenting"
          >
          Problem
          </TableHeader>

           <TableHeader
           name="Location Code"
           numeric
           tooltip="The location code of the incident"
          >
          Location Code
          </TableHeader>

           <TableHeader
           name="Demographics"
           sortFn={(a, b, isAsc) => (isAsc ? a : b).match(/(.*)/)[1].localeCompare((isAsc ? b : a).match(/(.*)/)[1])}
           tooltip="Patient Demographics"
          >
          Demographics
          </TableHeader>

           <TableHeader
           name="Rec"
           numeric
           tooltip="Time the call was received"
          >
          Received
          </TableHeader>

           <TableHeader
           name="EnR"
           numeric
           tooltip="Time departed en route to scene"
          >
          En Route
          </TableHeader>

           <TableHeader
           name="From"
           numeric
           tooltip="Time departed from the scene"
          >
          From Scene
          </TableHeader>

           <TableHeader
           name="Dest"
           numeric
           tooltip="Time arrived at the destination"
          >In
          Arrived
          </TableHeader>

           <TableHeader
           name="In S"
           numeric
           tooltip="Time in service"
          >
          In Service
          </TableHeader>

      </Table>

    );
  }
}

export default DataTable;
