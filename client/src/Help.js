import React from 'react';
import SiteFooter from './SiteFooter';

class Help extends React.Component {

  render(props) {
    return (
      <div>
        <article className="container">
          <h2>EMS Dashboard Help Manual</h2>
            <div className="helpManual-text">
              <p>
                The EMS Dashboard is a dashboard application that facilitates the visualization of inputted EMS call data,
                designed for Kevin Guo and the Cornell EMS team.
              </p>
              <p>
                This application is developed by Arpit Sheth (as3668@cornell.edu), Kevin Schaich (krs252@cornell.edu),
                Nate Smith (ncs77@cornell.edu), Gibson Mulonga (glm83@cornell.edu), Bomi Lee (bl557@cornell.edu), and Jerica Huang (jh2263@cornell.edu).
              </p>
            </div>
          <hr/>

          <h3>Overview</h3>
            <div className="helpManual-text">
            <p>
              The Overview section of the website is the main section of the website. All charts and apps reside on this page.
              There are seven visualizations on this page.
            </p>
            <p>
            The first shows the number of call responses in a given time range as a bar graph. The second shows the
            average response time between calls over the same time range as a line graph. The third is an hourly breakdown of the times when calls were received in the form of a bar chart. The fourth, fifth, and sixth are all pie charts detailing the most common chief complaints, most common dispositions, and
            The most common call types. The last is a map with large blue circles superimposed over it. All seven visualizations are interactive. By hovering over the bars
            on the bar graphs, it is possible to get an exact value for the number of responses, hovering over the line on a given month will provide a precise average response time, hovering over any pie chart slice will provide an exact number for the complaint, disposition, or call type
            in question, and hovering over a blue cirlce on the map
            provides an average number of incidents for that location as well as the name of the location, all within the specified time range. The map also supports a zoom feature as well as click-and-drag scrolling.
            </p>
            <p><img src= "/img/filter-date.png" /></p>
            <p>
              The box at the top of the page allows for date range specification using a calendar interface. Simply select the desired start and end dates and the visualizations will dynamically change to show how the data changes for that specific range.
            </p>
            </div>
          <hr/>

          <h3>View Table</h3>
            <div className="helpManual-text">
              <p>View Table section of the website displays the database's raw data in table format.
              The table contains many different values and thus implements a horizontal scrolling feature so all columns will be visible. Each column is also
              sortable.
              </p>
              <p><img src= "/img/table-sort.png" /></p>
              <p>
                Clicking on the column header once will sort the table alphabetically or in descending order and clicking it again will sort it in reverse alphabetiacal order or ascending numeric order.<br></br><br></br>
                Similarly to the ovierview page, the View Table page also has a box at the top of the page that allows for date range specification using a calendar interface, used by selecting a desired start and end date. The table
                then dynamically changes the visible entries.
              </p>


            </div>
          <hr/>

          <h3>Manage Data</h3>
            <div className="helpManual-text">
            The manage data section of the website allows the user to update the website's database.<br></br><br></br>

            Click the button that says "Select A File" and navigate to the excel file you want to upload, select it, and click the button that says "Upload".
            Then navigate to the Overview or View Table pages to perform the desired analyses.
            </div>
        </article>
        <SiteFooter/>
      </div>
    );
  }
}

export default Help;
