import request from 'superagent';

const parseJSON = (response) => {
  return response.json();
}

const checkStatus = (response) => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(`HTTP Error ${response.statusText}`);
  error.status = response.statusText;
  error.response = response;
  console.log(error);
  throw error;
}

const search = (params, cb) => {
  console.log(params);
  return request.get('/calls')
    // .set('Accept', 'application/json')
    .query(params);
}

const importData = (cb) => {
  return fetch('import', {
    accept: 'application/json',
  }).then(checkStatus)
    .then(parseJSON)
    .then(cb);
}

const DB = { search, importData };
export default DB;
