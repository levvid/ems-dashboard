import React from 'react';
import {Pie} from 'react-chartjs-2';


class PresentingProblemChart extends React.Component {

	/*Returns the number of days between start and end*/
	getDifferenceInDays(start, end) {
		var timeDiff = Math.abs(end.getTime() - start.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		return diffDays;
	}


	/*Returns the [min, max] dates for the data provided in props.calls*/
	getMaxAndMinDate(){
		var datesArray = [], tmpDate;
		for (var i = this.props.calls.length - 1; i >= 0; i--) {
			tmpDate = new Date(this.props.calls[i]['Date']);
			if(tmpDate){//check whether the date object is valid
				datesArray.push(tmpDate);
			}
		}
		var maxDate = new Date(Math.max.apply(null,datesArray));
		var minDate = new Date(Math.min.apply(null,datesArray));
		return [minDate, maxDate];
	}


	render(props) {
		var chiefComplaintsDict = {};
		var currentLabels = [];
		var currentData = [];

		for (var i = this.props.calls.length - 1; i >= 0; i--) {
			var chiefComplaint = this.props.calls[i]['Presenting Problem'];
			if(chiefComplaint in chiefComplaintsDict){
				chiefComplaintsDict[chiefComplaint] += 1;
			}else{
				chiefComplaintsDict[chiefComplaint] = 1;
			}



		}
		// Create items array
		var items = Object.keys(chiefComplaintsDict).map(function(key) {
			return [key, chiefComplaintsDict[key]];
		});

		// Sort the array based on the second element
		items.sort(function(first, second) {
			return second[1] - first[1];
		});

		// Create a new array with only the first 5 items
		items = items.slice(0, 5);

		for (var i = items.length - 1; i >= 0; i--) {
			currentLabels[i] = items[i][0];
			currentData[i] = items[i][1];
		}




		var pieData = {
			labels: currentLabels,
			datasets: [
			{
				data: currentData,
				backgroundColor: [
				'rgba(153, 102, 255, 0.7)',
				'rgba(255, 159, 64, 0.7)',
				'rgba(255,99,132,0.7)',
				'rgba(54, 162, 235, 0.7)',
				'rgba(255, 206, 86, 0.7)'
				],
				hoverBackgroundColor: [
				'rgba(153, 102, 255, 1)',
				'rgba(255, 159, 64, 1)',
				'rgba(255,99,132,1)',
				'rgba(54, 162, 235, 1)',
				'rgba(255, 206, 86, 1)'
				]
			}]
		};
		return (
			<div>
			<Pie data = {pieData} />
			</div>
			);

	}
}

export default PresentingProblemChart;
