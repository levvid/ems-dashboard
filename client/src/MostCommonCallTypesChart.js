import React from 'react';
import {Pie} from 'react-chartjs-2';


class MostCommonCallTypesChart extends React.Component {


	render(props) {
		var callTypesDict = {};
		var currentLabels = [];
		var currentData = [];

		for (var i = this.props.calls.length - 1; i >= 0; i--) {
			var callType = this.props.calls[i]['Call Type'];
			if(callType in callTypesDict){
				callTypesDict[callType] += 1;
			}else{
				callTypesDict[callType] = 1;
			}



		}
		// Create items array
		var items = Object.keys(callTypesDict).map(function(key) {
			return [key, callTypesDict[key]];
		});

		// Sort the array based on the second element
		items.sort(function(first, second) {
			return second[1] - first[1];
		});

		// Create a new array with only the first 5 items
		items = items.slice(0, 5);

		for (var i = items.length - 1; i >= 0; i--) {
			currentLabels[i] = items[i][0];
			currentData[i] = items[i][1];
		}

		var pieData = {
			labels: currentLabels,
			datasets: [
			{
				data: currentData,
				backgroundColor: [
				'rgba(153, 102, 255, 0.7)',
				'rgba(255, 159, 64, 0.7)',
				'rgba(255,99,132,0.7)',
				'rgba(54, 162, 235, 0.7)',
				'rgba(255, 206, 86, 0.7)'
				],
				hoverBackgroundColor: [
				'rgba(153, 102, 255, 1)',
				'rgba(255, 159, 64, 1)',
				'rgba(255,99,132,1)',
				'rgba(54, 162, 235, 1)',
				'rgba(255, 206, 86, 1)'
				]
			}]
		};
		return (
			<div>
			<Pie data = {pieData} options="legend=false"/>
			</div>
			);

	}
}

export default MostCommonCallTypesChart;
