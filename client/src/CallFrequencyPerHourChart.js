import React from 'react';
import {Bar} from 'react-chartjs-2';
import {Button} from 'react-mdl';

class CallFrequencyPerHourChart extends React.Component {

	state = {
		day: 1000
	}

	setDay = (day) => {
		this.setState({day: day})
	}

	getDay(){
		var items = [];
		if(this.state.day == 1000){
			items = [];
			items = this.props.calls;
		}
		else if(this.state.day == 0){
			items = [];
			for (var i = this.props.calls.length - 1; i >= 0; i--) {

				if(new Date(this.props.calls[i]['Date']).getDay() == this.state.day){
					console.log("THis is hte day: " + new Date(this.props.calls[i]['Date']).getDay() + " and this is hte state day: " + this.state.day);
					items[i] = this.props.calls[i];
				}
			}
		}
		return items;
	}

	render(props) {
		var timeReceivedDict = {};
		var currentLabels = [];
		var currentData = [];
		var currentCalls = this.props.calls;
		//currentCalls = this.getDay();
		console.log("Length: " + currentCalls.length);
		for(var j = 0; j < 24; j++){
			if (j < 9) {
				currentLabels[j] = "0" + j + ":00 - " + "0" + (j+1) + ":00";
			}
			else if(j == 9){
				currentLabels[j] = "09:00 - 10:00";
			}
			else{
				currentLabels[j] = j + ":00 - " + (j+1) + ":00";
			}
			currentData[j] = 0;

		}

		for (var i = currentCalls.length - 1; i >= 0; i--) {
			var timeReceived = currentCalls[i]['Rec'];
			console.log("Day is: " + new Date(currentCalls[i]['Date']).getDay());
			var hourReceived = timeReceived.substring(0,2);
			if(this.state.day == 1000 || new Date(currentCalls[i]['Date']).getDay() == this.state.day){
				currentData[parseInt(hourReceived)] += 1;
			}

		}

		var barData = {
			labels: currentLabels,
			datasets: [
			{
				label: 'Call Frequency',
				data: currentData,
				backgroundColor: [
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)',
					'rgba(57, 144, 229, 0.2)'
				],
				hoverBackgroundColor: [
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)',
					'rgba(57, 144, 229, 0.7)'
				],
				borderColor: [
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)',
					'rgba(57, 144, 229, 1)'
				],
				borderWidth: 1
			}]
		};
		return (
			<div>
				<Bar data = {barData} />
				<Button raised ripple onClick={() => {this.setDay(0)}}>Mon</Button>&emsp;
				<Button raised ripple onClick={() => {this.setDay(1)}}>Tue</Button>&emsp;
				<Button raised ripple onClick={() => {this.setDay(2)}}>Wed</Button>&emsp;
				<Button raised ripple onClick={() => {this.setDay(3)}}>Thu</Button>&emsp;
				<Button raised ripple onClick={() => {this.setDay(4)}}>Fri</Button>&emsp;
				<Button raised ripple onClick={() => {this.setDay(5)}}>Sat</Button>&emsp;
				<Button raised ripple onClick={() => {this.setDay(6)}}>Sun</Button>&emsp;
				<Button raised ripple colored onClick={() => {this.setDay(1000)}}>Show All</Button>
			</div>
			);

		}
	}

	export default CallFrequencyPerHourChart;
