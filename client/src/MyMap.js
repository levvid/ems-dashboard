import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import {Tooltip} from 'react-mdl';
import request from 'superagent';

class Circle extends Component {
  render() {
    return (
      <div className='map-datapoint' style={{
          transform: 'translate(-50%, -50%)',
          width: Math.sqrt(this.props.calls.calls.length) * 30 + 'px'
        }}>
          <div className='map-tooltip'>{this.props.calls.name}:<br></br>{this.props.calls.calls.length} Calls</div>
          <div className='circle'
            style={{
              width: Math.sqrt(this.props.calls.calls.length) * 30 + 'px',
              height: Math.sqrt(this.props.calls.calls.length) * 30 + 'px',
              border: '2px solid rgba(239, 84, 84, .75)',
              background: 'rgba(239, 84, 84, .5)',
              borderRadius: '50%'
          }}>
          </div>
        </div>
    )
  }
}

export default class MyMap extends Component {
  getCalls = () => {
    var callsByLocation = {}
    this.props.calls.forEach(function(call) {
      if (callsByLocation[call.googleLocation]) {
        callsByLocation[call.googleLocation].calls.push(call);
      }
      else {
        if (call.googleLocation) {
          callsByLocation[call.googleLocation] = {
            calls: [call],
            name: call.googleLocation,
            lat: call.lat,
            lng: call.lng
          };
        }
      }
    });
    return callsByLocation;
  }

  render() {
    const calls = this.getCalls();
    const callKeys = Object.keys(calls);
    const totalNumCalls = callKeys.length;
    const circles = callKeys.map(key => {
              return (
                <Circle
                  lat={calls[key].lat}
                  lng={calls[key].lng}
                  totalNumCalls={totalNumCalls}
                  calls={calls[key]}
                />
              )
            })
    console.log(calls);
    return (
      <div style={{width: '100%', height: 'calc(100vh - 130px)'}}>
      <GoogleMapReact
        defaultCenter={{lat: 42.4469782, lng: -76.4778971}}
        defaultZoom={15}
      >
      {circles}
      </GoogleMapReact>
      </div>
    );
  }
}
