import React from 'react';
import { DateRangePicker } from 'react-dates';
import './react-dates.css';
import {
  Button
} from 'react-mdl'

class FilterBar extends React.Component {
  state = {
    startDate: null,
    endDate: null,
    focusedInput: null
  }

  handleDatesChange = ({startDate, endDate}) => {
    if(startDate) {
      this.props.parent.setState(
        {'startDate': startDate.toDate()},
        this.props.parent.updateData
      );
    }
    if(endDate) {
      this.props.parent.setState(
        {'endDate': endDate.toDate()},
        this.props.parent.updateData
      );
    }
    this.setState({ startDate, endDate })
  }

  render() {
    return (
      <div className='filterbar'>
        <div className='mdl-layout__header-row'>

          <DateRangePicker
            startDate={this.state.startDate} // momentPropTypes.momentObj or null,
            endDate={this.state.endDate} // momentPropTypes.momentObj or null,
            onDatesChange={this.handleDatesChange} // PropTypes.func.isRequired,
            focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
            onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
            isOutsideRange={() => false}
            displayFormat="MMM D, YYYY"
          />
        </div>



      </div>
    );
  }
}

export default FilterBar;
