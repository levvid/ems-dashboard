import React from 'react';
import {Bar} from 'react-chartjs-2';


class TotalNumberOfCallsChart extends React.Component {

	/*Returns the number of days between start and end*/
	getDifferenceInDays(start, end) {
		var timeDiff = Math.abs(end.getTime() - start.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		return diffDays;
	}


	/*Returns the [min, max] dates for the data provided in props.calls*/
	getMaxAndMinDate(){
		var datesArray = [], tmpDate;
		for (var i = this.props.calls.length - 1; i >= 0; i--) {
			tmpDate = new Date(this.props.calls[i]['Date']);
			if(tmpDate){//check whether the date object is valid
				datesArray.push(tmpDate);
			}
		}
		var maxDate = new Date(Math.max.apply(null,datesArray));
		var minDate = new Date(Math.min.apply(null,datesArray));
		return [minDate, maxDate];
	}


	render(props) {
		 var totalNumberOfCalls = [];
		 var maxAndMinDate = this.getMaxAndMinDate();
		 var startDate = maxAndMinDate[0], endDate = maxAndMinDate[1], startDateCopy = maxAndMinDate[0];
		 var currentLabels = [];
		 var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		 var monthsUsed = [];
		 var numberOfDaysToPlot = this.getDifferenceInDays(startDate, endDate);

		 if( numberOfDaysToPlot < 46  ){
		 	var i = 0;
		 	for(var m = startDateCopy; m <= endDate; m.setDate(m.getDate() + 1)) {
		 		currentLabels[i] = months[m.getMonth()] + " " + m.getDate();
		 		totalNumberOfCalls[i] = 0;
		 		i += 1;
		 	}
		 }
		 else if(numberOfDaysToPlot > 46 ){
		 	var i = 0;
		 	for(var m = startDate.getMonth(); m <= endDate.getMonth(); m = (m + 1)) {
		 		currentLabels[i] = months[m];
		 		monthsUsed[i] = m;
		 		totalNumberOfCalls[i] = 0;
		 		i += 1;
		 	}
		 }

		 for (var i = this.props.calls.length - 1; i >= 0; i--) {
		 	var dateOfCall = new Date(this.props.calls[i]['Date']);
		 	var monthOfCall = dateOfCall.getMonth();


		 	if(numberOfDaysToPlot < 46){
		 		totalNumberOfCalls[this.getDifferenceInDays(startDate, dateOfCall)] += 1;
		 	}
		 	else{
		 		totalNumberOfCalls[monthsUsed.indexOf(monthOfCall)] += 1;
		 	}
		 }


		 const barData = {
		 	labels: currentLabels,
		 	datasets: [{
		 		label: 'Total Number of Calls',
		 		data: totalNumberOfCalls,
		 		backgroundColor: [
		 		'rgba(67, 67, 67, 0.2)',
		 		'rgba(67, 67, 67, 0.2)',
		 		'rgba(67, 67, 67, 0.2)',
		 		'rgba(67, 67, 67, 0.2)',
		 		'rgba(67, 67, 67, 0.2)',
		 		'rgba(67, 67, 67, 0.2)',
		 		'rgba(67, 67, 67, 0.2)',
		 		'rgba(67, 67, 67, 0.2)',
		 		'rgba(67, 67, 67, 0.2)',
		 		'rgba(67, 67, 67, 0.2)',
		 		'rgba(67, 67, 67, 0.2)',
		 		'rgba(67, 67, 67, 0.2)'
		 		],
		 		hoverBackgroundColor: [
		 		'rgba(67, 67, 67,0.7)',
		 		'rgba(67, 67, 67,0.7)',
		 		'rgba(67, 67, 67,0.7)',
		 		'rgba(67, 67, 67,0.7)',
		 		'rgba(67, 67, 67,0.7)',
		 		'rgba(67, 67, 67,0.7)',
		 		'rgba(67, 67, 67,0.7)',
		 		'rgba(67, 67, 67,0.7)',
		 		'rgba(67, 67, 67,0.7)',
		 		'rgba(67, 67, 67,0.7)',
		 		'rgba(67, 67, 67,0.7)',
		 		'rgba(67, 67, 67,0.7)'
		 		],
		 		borderColor: [
		 		'rgba(67, 67, 67,1)',
		 		'rgba(67, 67, 67,1)',
		 		'rgba(67, 67, 67,1)',
		 		'rgba(67, 67, 67,1)',
		 		'rgba(67, 67, 67,1)',
		 		'rgba(67, 67, 67,1)',
		 		'rgba(67, 67, 67,1)',
		 		'rgba(67, 67, 67,1)',
		 		'rgba(67, 67, 67,1)',
		 		'rgba(67, 67, 67,1)',
		 		'rgba(67, 67, 67,1)',
		 		'rgba(67, 67, 67,1)'
		 		],
		 		borderWidth: 1
		 	}]
		 };
		 return (
		 	<div>
		 		<Bar data={barData} />
		 	</div>
		 	);

		}
	}

	export default TotalNumberOfCallsChart;
