import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
} from 'react-router-dom';
import {
  Footer,
  FooterSection,
  FooterLinkList
} from 'react-mdl'

class SiteFooter extends React.Component {
  render() {
    return (
      <Footer size="mini">
          <FooterSection type="left" logo="EMS Dashboard">
              <FooterLinkList>
                  <Link to="/">Home</Link>
                  <Link to="/manage">Manage</Link>
                  <Link to="/help">Help</Link>
              </FooterLinkList>
          </FooterSection>
      </Footer>
    );
  }
}

export default SiteFooter;
