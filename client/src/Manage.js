import React from 'react';
import Dropzone from 'react-dropzone';
import request from 'superagent';
import SiteFooter from './SiteFooter';
import { Button } from 'react-mdl';

class Manage extends React.Component {

  onDrop = (files) => {
    var ReactDOM = require('react-dom');

    if (files.length === 1) {
      this.setState({files: files});
    }

    ReactDOM.render(
      <div>File selected!</div>,
      document.getElementById('fileUploaded')
    );
  }

  handleFileUpload = () => {
    if(this.state == null){
      window.alert("ERROR: No file uploaded.");
    } else {
      var req = request.post('/import');
      if(this.state.files[0].type.toString().endsWith(".sheet")){
        req.attach('upload', this.state.files[0]);
        req.end(console.log);
        window.alert("File successfully uploaded!");
      } else{
        window.alert("ERROR: File uploaded was not of xlsx format.");
      }
    }

  }

  render(props) {
    return (
      <div>
          <div className="container">
          <h1>Manage Data</h1>
          <hr/>
          <div className='dropzone-wrap'>
            <Dropzone onDrop={this.onDrop} >
              <div className='text-label'>
                Drag & drop spreadsheet here.
              </div>
              <Button raised>Select A File</Button>
            </Dropzone>
          </div>
          <p id = "fileUploaded"><big>No file uploaded</big></p>
          <Button raised colored onClick={this.handleFileUpload}>Upload</Button>
        </div>
        {/*<SiteFooter />*/}
      </div>
    );
  }
}

export default Manage;
