import React from 'react';
import {Line} from 'react-chartjs-2';

class AverageResponseTime extends React.Component {


  /*Returns the time difference in minutes between start and end times*/
  getTimeDifference(start, end) {
    var beginDate = new Date(0, 0, 0, start.substring(0,2), start.substring(2,4), 0);
    var stopDate = new Date(0, 0, 0, end.substring(0,2), end.substring(2,4), 0);
    var diff = stopDate.getTime() - beginDate.getTime();
    var minutes = Math.floor(diff / 1000 / 60);
    return minutes;
  }

  /*Returns the [min, max] dates for the data provided in props.calls*/
  getMaxAndMinDate(){
    var datesArray = [], tmpDate;
    for (var i = this.props.calls.length - 1; i >= 0; i--) {
      tmpDate = new Date(this.props.calls[i]['Date']);
      if(tmpDate){//check whether the date object is valid
        datesArray.push(tmpDate);
      }
    }
    var maxDate = new Date(Math.max.apply(null,datesArray));
    var minDate = new Date(Math.min.apply(null,datesArray));
    return [minDate, maxDate];
  }

  /*Returns the number of days between start and end*/
  getDifferenceInDays(start, end) {
    var timeDiff = Math.abs(end.getTime() - start.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    return diffDays;
  }


  render(props) {
   var callsPerMonth = [], totalResponseTimePerMonth = [], averageResponseTimePerMonth = [];


   var maxAndMinDate = this.getMaxAndMinDate();
   var startDate = maxAndMinDate[0], endDate = maxAndMinDate[1], startDateCopy = maxAndMinDate[0];
   var currentLabels = [];
   var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
   var monthsUsed = [];
   var numberOfDaysToPlot = this.getDifferenceInDays(startDate, endDate);

   if( numberOfDaysToPlot < 46  ){
    var i = 0;
    for(var m = startDateCopy; m <= endDate; m.setDate(m.getDate() + 1)) {
      currentLabels[i] = months[m.getMonth()] + " " + m.getDate();
      callsPerMonth[i] = 0;
      totalResponseTimePerMonth[i] = 0;
      averageResponseTimePerMonth[i] = 0;
      i += 1;
    }
  }

  else if(numberOfDaysToPlot > 46 ){
    var i = 0;
    for(var m = startDate.getMonth(); m <= endDate.getMonth(); m = (m + 1)) {
      currentLabels[i] = months[m];
      monthsUsed[i] = m;
      callsPerMonth[i] = 0;
      totalResponseTimePerMonth[i] = 0;
      averageResponseTimePerMonth[i] = 0;
      i += 1;
    }
  }

  for (var i = this.props.calls.length - 1; i >= 0; i--) {
    var dateOfCall = new Date(this.props.calls[i]['Date']);
    var monthOfCall = dateOfCall.getMonth();
    var timeReceived = String(this.props.calls[i]['Rec']);
    var timeFromScene = String(this.props.calls[i]['OS']);


    if(numberOfDaysToPlot < 46){
      var index = this.getDifferenceInDays(startDate, dateOfCall);
      callsPerMonth[index] += 1;
      if(timeReceived && timeFromScene){
        totalResponseTimePerMonth[index] += this.getTimeDifference(timeReceived, timeFromScene);
      }

    }
    else{
      var monthIndex = monthsUsed.indexOf(monthOfCall);
      callsPerMonth[monthIndex] += 1;
      if(timeReceived && timeFromScene){
        totalResponseTimePerMonth[monthIndex] += this.getTimeDifference(timeReceived, timeFromScene);
      }
    }
  }



  for (var i = callsPerMonth.length - 1; i >= 0; i--) {
    if(callsPerMonth[i] != 0){//avoid division by zero
      averageResponseTimePerMonth[i] += totalResponseTimePerMonth[i]/callsPerMonth[i];
    }
  }


  const lineData = {
    labels: currentLabels,
    datasets: [
    {
      label: 'Average Response Time',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgb(87, 193, 130,0.4)',
      borderColor: 'rgb(87, 193, 130,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgb(87, 193, 130,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgb(87, 193, 130,1)',
      pointHoverBorderColor: 'rgb(67, 67, 67)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: averageResponseTimePerMonth
    }
    ]
  };

  /***********************End of Average Response Time************************************/



  var r = [];
  	return (
      <div>
        <Line data={lineData} />
      </div>
      );
    }
  }

  export default AverageResponseTime;
