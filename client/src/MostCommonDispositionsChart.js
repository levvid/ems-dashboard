import React from 'react';
import {Doughnut} from 'react-chartjs-2';


class MostCommonDispositionsChart extends React.Component {


	render(props) {
		var dispositionsDict = {};
		var currentLabels = [];
		var currentData = [];

		for (var i = this.props.calls.length - 1; i >= 0; i--) {
			var disposition = this.props.calls[i]['Disposition'];
			if(disposition in dispositionsDict){
				dispositionsDict[disposition] += 1;
			}else{
				dispositionsDict[disposition] = 1;
			}



		}
		// Create items array
		var items = Object.keys(dispositionsDict).map(function(key) {
			return [key, dispositionsDict[key]];
		});

		// Sort the array based on the second element
		items.sort(function(first, second) {
			return second[1] - first[1];
		});

		// Create a new array with only the first 5 items
		items = items.slice(0, 5);

		for (var i = items.length - 1; i >= 0; i--) {
			currentLabels[i] = items[i][0];
			currentData[i] = items[i][1];
		}




		var pieData = {
			labels: currentLabels,
			datasets: [
			{
				data: currentData,
				backgroundColor: [
				'rgba(255, 99, 132, 0.7)',
		 		'rgba(54, 162, 235, 0.7)',
		 		'rgba(255, 206, 86, 0.7)',
		 		'rgba(75, 192, 192, 0.7)',
		 		'rgba(153, 102, 255, 0.7)',
		 		'rgba(255, 159, 64, 0.7)',
		 		'rgba(255, 99, 132, 0.7)',
		 		'rgba(54, 162, 235, 0.7)',
		 		'rgba(255, 206, 86, 0.7)',
		 		'rgba(75, 192, 192, 0.7)',
		 		'rgba(153, 102, 255, 0.7)',
		 		'rgba(255, 159, 64, 0.7)',
				'rgba(255, 99, 132, 0.7)',
		 		'rgba(54, 162, 235, 0.7)',
		 		'rgba(255, 206, 86, 0.7)',
		 		'rgba(75, 192, 192, 0.7)',
		 		'rgba(153, 102, 255, 0.7)',
		 		'rgba(255, 159, 64, 0.7)',
		 		'rgba(255, 99, 132, 0.7)',
		 		'rgba(54, 162, 235, 0.7)',
		 		'rgba(255, 206, 86, 0.7)',
		 		'rgba(75, 192, 192, 0.7)',
		 		'rgba(153, 102, 255, 0.7)',
		 		'rgba(255, 159, 64, 0.7)'
				],
				hoverBackgroundColor: [
				'rgba(255,99,132,1.0)',
		 		'rgba(54, 162, 235, 1.0)',
		 		'rgba(255, 206, 86, 1.0)',
		 		'rgba(75, 192, 192, 1.0)',
		 		'rgba(153, 102, 255, 1.0)',
		 		'rgba(255, 159, 64, 1.0)',
		 		'rgba(255,99,132,1.0)',
		 		'rgba(54, 162, 235, 1.0)',
		 		'rgba(255, 206, 86, 1.0)',
		 		'rgba(75, 192, 192, 1.0)',
		 		'rgba(153, 102, 255, 1.0)',
		 		'rgba(255, 159, 64, 1.0)',
		 		'rgba(255,99,132,1.0)',
		 		'rgba(54, 162, 235, 1.0)',
		 		'rgba(255, 206, 86, 1.0)',
		 		'rgba(75, 192, 192, 1.0)',
		 		'rgba(153, 102, 255, 1.0)',
		 		'rgba(255, 159, 64, 1.0)',
		 		'rgba(255,99,132,1.0)',
		 		'rgba(54, 162, 235, 1.0)',
		 		'rgba(255, 206, 86, 1.0)',
		 		'rgba(75, 192, 192, 1.0)',
		 		'rgba(153, 102, 255, 1.0)',
		 		'rgba(255, 159, 64, 1.0)'
				]
			}]
		};
		return (
			<div>
			<Doughnut data = {pieData} />
			</div>
			);

	}
}

export default MostCommonDispositionsChart;
