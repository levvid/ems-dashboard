import React from 'react';
import Home from './Home';
import Dispatch from './Dispatch';
import MapView from './MapView';
import DataTable from './DataTable';
import Manage from './Manage';
import Help from './Help';
import FilterBar from './FilterBar';
import {
  BrowserRouter as Router,
  Route,
  Link,
} from 'react-router-dom';
import 'react-mdl/extra/material.css';
import 'react-mdl/extra/material.js';
import {
  Layout,
  Header,
  Navigation,
  Content,
  Icon
} from 'react-mdl';
import request from 'superagent';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: null,
      endDate: null,
      calls: []
    }
  }

  componentDidMount = () => {
    this.updateData();
  }

  updateData = () => {
    var params = {
      'startDate': null,
      'endDate': null
    }
    if (this.state.startDate) {
      params['startDate'] = this.state.startDate.toString()
    }
    if (this.state.endDate) {
      params['endDate'] = this.state.endDate.toString()
    }
    request.get('/calls')
    .query(params)
    .end((err, calls) => {
      this.setState({calls: calls.body});
    });
  }

  render() {
    return (
      <Router>
        <div>
          {/* Simple header with fixed tabs. */}
          <div>
            <Layout fixedHeader>
              <Header title={<Link to="/">EMS Dashboard</Link>}>
                  <Navigation>
                    <Link to="/">Overview</Link>
                    <Link to="/dispatch">Dispatch</Link>
                    <Link to="/map">Map</Link>
                    <Link to="/table">Table</Link>
                    <Link to="/manage">Manage</Link>
                    <Link to="/help"><Icon name="help" /></Link>
                  </Navigation>
              </Header>
              <Content>
                <FilterBar parent={this}/>
                <Route exact path='/' render={() => <Home calls={this.state.calls}/>}/>
                <Route path='/dispatch'  render={() => <Dispatch calls={this.state.calls} />}/>
                <Route path='/map'  render={() => <MapView calls={this.state.calls} />}/>
                <Route path='/table'  render={() => <div className="container-dataTable"><DataTable calls={this.state.calls}/></div>}/>
                <Route path='/manage' render={() => <Manage calls={this.state.calls}/>}/>
                <Route path='/help' render={() => <Help calls={this.state.calls}/>}/>
              </Content>
            </Layout>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
