import React from 'react';
import MyMap from './MyMap';
import {
} from 'react-mdl'

class MapView extends React.Component {
  render(props) {
    return (
      <div>
        <MyMap calls={this.props.calls} />
      </div>
    );
  }
}

export default MapView;
