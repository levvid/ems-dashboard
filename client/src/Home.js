import React from 'react';
import TotalNumberOfCallsChart from './TotalNumberOfCallsChart';
import AverageResponseTimeChart from './AverageResponseTimeChart';
import AverageServiceTimeChart from './AverageServiceTimeChart';
import CallFrequencyPerHourChart from './CallFrequencyPerHourChart';
import SiteFooter from './SiteFooter';
import {
  Grid,
  Cell
} from 'react-mdl';


class Home extends React.Component {

  /*Returns the time difference in minutes between start and end times*/
  getTimeDifference(start, end) {
    var beginDate = new Date(0, 0, 0, start.substring(0,2), start.substring(2,4), 0);
    var stopDate = new Date(0, 0, 0, end.substring(0,2), end.substring(2,4), 0);
    var diff = stopDate.getTime() - beginDate.getTime();
    var minutes = Math.floor(diff / 1000 / 60);
    return minutes;
  }

  /*Returns the [min, max] dates for the data provided in props.calls*/
  getMaxAndMinDate(){
    var datesArray = [], tmpDate;
    for (var i = this.props.calls.length - 1; i >= 0; i--) {
      tmpDate = new Date(this.props.calls[i]['Date']);
      if(tmpDate){//check whether the date object is valid
        datesArray.push(tmpDate);
      }
    }
    var maxDate = new Date(Math.max.apply(null,datesArray));
    var minDate = new Date(Math.min.apply(null,datesArray));
    return [minDate, maxDate];
  }

  /*Returns the number of days between start and end*/
  getDifferenceInDays(start, end) {
    var timeDiff = Math.abs(end.getTime() - start.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    return diffDays;
  }

  render(props) {
    var callsPerMonth = [], totalResponseTimePerMonth = [], averageResponseTimePerMonth = [];

    var maxAndMinDate = this.getMaxAndMinDate();
    var startDate = maxAndMinDate[0], endDate = maxAndMinDate[1], startDateCopy = maxAndMinDate[0];
    var currentLabels = [];
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var monthsUsed = [];
    var numberOfDaysToPlot = this.getDifferenceInDays(startDate, endDate);

    var OverviewTotalCalls = this.props.calls.length;
    var OverviewAvgRespTimeAcc = 0;
    var OverviewAvgRespTime = 0;
    var OverviewTotalTransported = 0;

    if( numberOfDaysToPlot < 46  ){
     var i = 0;
     for(var m = startDateCopy; m <= endDate; m.setDate(m.getDate() + 1)) {
       currentLabels[i] = months[m.getMonth()] + " " + m.getDate();
       callsPerMonth[i] = 0;
       totalResponseTimePerMonth[i] = 0;
       averageResponseTimePerMonth[i] = 0;
       i += 1;
     }
   }

   else if(numberOfDaysToPlot > 46 ){
     var i = 0;
     for(var m = startDate.getMonth(); m <= endDate.getMonth(); m = (m + 1)) {
       currentLabels[i] = months[m];
       monthsUsed[i] = m;
       callsPerMonth[i] = 0;
       totalResponseTimePerMonth[i] = 0;
       averageResponseTimePerMonth[i] = 0;
       i += 1;
     }
   }

   for (var i = this.props.calls.length - 1; i >= 0; i--) {
     var dateOfCall = new Date(this.props.calls[i]['Date']);
     var monthOfCall = dateOfCall.getMonth();
     var timeReceived = String(this.props.calls[i]['Rec']);
     var timeFromScene = String(this.props.calls[i]['OS']);
     console.log("timeReceived: " + timeReceived.substring(0,2));
     console.log("timeFromScene: " + timeFromScene);


     if(numberOfDaysToPlot < 46){
       var index = this.getDifferenceInDays(startDate, dateOfCall);
       callsPerMonth[index] += 1;
       if(timeReceived && timeFromScene){
         totalResponseTimePerMonth[index] += this.getTimeDifference(timeReceived, timeFromScene);
       }

     }
     else{
       var monthIndex = monthsUsed.indexOf(monthOfCall);
       callsPerMonth[monthIndex] += 1;
       if(timeReceived && timeFromScene){
         totalResponseTimePerMonth[monthIndex] += this.getTimeDifference(timeReceived, timeFromScene);
       }
     }
   }



   for (var i = callsPerMonth.length - 1; i >= 0; i--) {
     if(callsPerMonth[i] != 0){//avoid division by zero
       averageResponseTimePerMonth[i] += totalResponseTimePerMonth[i]/callsPerMonth[i];
     }
   }

   for (var i = 0; i < averageResponseTimePerMonth.length; i++) {
     OverviewAvgRespTimeAcc += averageResponseTimePerMonth[i];
   }
   if (averageResponseTimePerMonth.length != 0) {
     OverviewAvgRespTime = Math.abs(OverviewAvgRespTimeAcc / averageResponseTimePerMonth.length);
   }
   if (Number.isNaN(OverviewAvgRespTime)) {
     OverviewAvgRespTime = 512;
   }



   var dispositionsDict = {};
   var currentLabels = [];
   var currentData = [];

   for (var i = this.props.calls.length - 1; i >= 0; i--) {
     var disposition = this.props.calls[i]['Disposition'];
     if(disposition in dispositionsDict){
       dispositionsDict[disposition] += 1;
     }else{
       dispositionsDict[disposition] = 1;
     }



   }
   // Create items array
   var dispositionItems = Object.keys(dispositionsDict).map(function(key) {
     if (key === "TRANSPORT") {
       OverviewTotalTransported = dispositionsDict[key];
     }
     return [key, dispositionsDict[key]];
   });

   // Sort the array based on the second element
   dispositionItems.sort(function(first, second) {
     return second[1] - first[1];
   });

   console.log(dispositionItems);




    return (
      <div>
        <div className="overviewPanel">
          <div className="container">
            <Grid>
              <Cell col={4}>
                <h1>{OverviewTotalCalls}<br/>
                <small>Total Calls</small></h1>
              </Cell>
              <Cell col={4}>
                <h1>{parseFloat(OverviewAvgRespTime / 60).toFixed(1)} m<br/>
                <small>Avg Response Time</small></h1>
              </Cell>
              <Cell col={4}>
              <h1>{OverviewTotalTransported}<br/>
              <small>Total Transported</small></h1>
              </Cell>
            </Grid>
          </div>
        </div>

        <div  style={{textAlign: 'center' }} className="container">

          <section className="chartSection">
            <h2>Number of Calls</h2>
            <TotalNumberOfCallsChart calls={this.props.calls} />
          </section>

          <section className="chartSection chartSection__alt">
            <Grid>
              <Cell col={6}>
              <h3>Average Response Time</h3>
              <AverageResponseTimeChart calls={this.props.calls} />
              </Cell>
              <Cell col={6}>
                <h3>Average Service Time</h3>
                <AverageServiceTimeChart calls={this.props.calls} />
              </Cell>
            </Grid>
          </section>

          <section className="chartSection">
            <h3>Peak Call Times Per Hour</h3>
            <CallFrequencyPerHourChart calls={this.props.calls} />
          </section>
        </div>
        {/*<MyMap calls={this.props.calls} />*/}

        <SiteFooter />
      </div>
      );
    }
  }



  export default Home;
