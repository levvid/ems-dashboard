# EMS Dashboard

CUEMS Dashboard was created using create-react-app, Babel, Node.js, and express. Below are some preliminary installation instructions that we have found helpful and compiled for our group members.

# Installation/Usage

### Windows Users Only

**Note:** If you encounter errors during installation, give [CMDer](http://cmder.net/) a try.

1. Install [Node.js](https://docs.mongodb.com/manual/installation/) (if you don't already have it).

2. Install [Yarn](https://yarnpkg.com/en/docs/install#windows-tab) (if you don't already have it).

3. Install [MongoDB](https://docs.mongodb.com/manual/installation/)  (if you don't already have it).

    If the installation doesn't work, you may need to [set up MongoDB environment](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/#run-mongodb-community-edition) first. Follow the instructions to Set up the MongoDB environment, Start MongoDB, and Connect to MongoDB.

    If you aren't already running the mongo interface you can run it by:

    `"C:\Program Files\MongoDB\Server\3.4\bin\mongo.exe"`

### Mac Users Only

1. Make sure you have [Homebrew](https://brew.sh/) installed.

2. Install Node:

    `brew install node`

3. Install Yarn:

    `brew install yarn`

4. Install MongoDB:

    ```
    brew install mongodb
    brew services start mongodb
    ```

### All Users (after above steps completed):

1. Clone repo & install dependencies:
    ```
    git clone https://github.com/kevinschaich/ems-dashboard
    cd ems-dashboard
    yarn install
    cd client
    yarn install
    cd ..
    ```

2. [OPTIONAL CHECKPOINT] Populate the database with some test rows:

    `pip install pymongo`
    `python populate_db.py`

3. Run the server from the **root directory** of the repo: `yarn start`

4. If you visit [http://localhost:3000](http://localhost:3000), you should see items from your DB being populated on the page!
