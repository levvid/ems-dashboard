import datetime
import time
import random
import string
import json
import sys
import os

from pymongo import MongoClient

db_uri = (os.environ.get('MONGODB_URI') or 'localhost:27017/cuems').rsplit('/', 1)

db_client = MongoClient(db_uri[0])
db = db_client[db_uri[1]]

print "DB URI", db_uri
print "DB_client", db_client
print "db", db

first_name = ["AARON", "ABE", "ALFRED", "ANDRE", "CAROL", "SUSAN",
        "BOB", "BOMI", "ARPIT", "KEVIN", "JERICA", "GIBSON", "NATE", "DARLA",
        "ANDREW", "PAUL", "SARAH", "ELIZABETH", "JASON", "EMMA", "GINGER",
        "LOUIS", "ROBERT"]
last_name = ["SMITH", "JOHNSON", "JONES", "BROWN", "LEE", "KIM", "SHETH", "CHANG", "SCHAICH",
       "MULONGA", "HUANG", "GUO", "SCOTT", "ADAMS", "BROWN", "GREEN", "WEST", "SANCHEZ",
       "MORRIS", "REED", "TURNER", "SIMPSON", "ANTHONY", "JENNINGS"]
hospital_locations = ["CORNELL HEALTH", "FIVE STAR URGENT CARE ITHACA", "CAYUGA MEDICAL CENTER",
       "CORTLAND REGIONAL MEDICAL CENTER"]
dispositions = ["TRANSPORT", "RMA", "CANCELLED"]
transport = ["HELICOPTER", "AMBULANCE"]
locations = ["HO PLAZA", "NORTH CAMPUS", "WEST CAMPUS", "COLLEGETOWN", "DUFFIELD HALL", "STATLER HALL",
       "ITHACA COLLEGE", "ITHACA COMMONS", "CHIPOTLE"]
injuries = ["HEART ATTACK", "STROKE", "BLUNT TRAUMA", "CAR CRASH", "DRUNK", "STUB TOE", "BLOODY NOSE"]
location_code = ["5401", "5402", "5403"]
bools = ["YES", "NO"]
air = ["YES", "NO", "CANCELLED"]
demographics = ["FULL", "PARTIAL", "NO"]
types = ["MEDICAL", "TRAUMA", "EVENT STANDBY", "NO PATIENT FOUND", "CANCELLED", "OTHER"]
equipment = ["CRAVAT", "BANDAID", "POCKET KNIFE", "OXYGEN KIT", "DEFIBRILLATOR", "INFUSION PUMP"]

def gen_callid():
  id = []
  for i in range(9):
    id.append(random.choice(range(1, 9)))
  return "".join(str(x) for x in id)

def gen_datetime():
  return datetime.datetime.strptime('{} {}'.format(random.randint(1, 366), 2017), '%j %Y')

def gen_time():
  h = random.randint(1, 24)
  h = str(h) if h >= 10 else str(0) + str(h)
  m = random.randint(0, 60)
  m = str(m) if m >= 10 else str(0) + str(m)
  return h + m


def gen_travel_time():
  secs_to_add = random.randint(120, 3600)
  currenttime = datetime.datetime.now()
  finaltime = currenttime + datetime.timedelta(seconds=secs_to_add)
  return finaltime.strftime('%H:%M:%S')

def gen_name():
  return random.choice(last_name)

def gen_latitude():
  return random.uniform(41.0, 43.0)

def gen_longitude():
  return random.uniform(75.0, 77.5)

def gen_hospital_location():
  return random.choice(hospital_locations)

def gen_location():
  return random.choice(locations)

def gen_disposition():
  return random.choice(dispositions)

def gen_transport():
  return random.choice(transport)

def gen_injury():
  return random.choice(injuries)

def gen_bool():
  return random.choice(bools)

def gen_type():
  return random.choice(types)

def gen_air():
  return random.choice(air)

def gen_demographics():
  return random.choice(demographics)

def gen_code():
  return random.choice(location_code)

def gen_equipment():
  return random.choice(equipment)

def gen_example_call():
  date = gen_datetime()
  time_t = gen_time()
  call = {
    'Run #': "17" + "-" + str(random.choice(range(100, 999))),
    'PCR #': gen_callid(),
    'Date': date,
    'CQI': gen_bool(),
    'Crew Chief': gen_name(),
    'Crew': gen_name() + "/" + gen_name(),
    'Driver': gen_name(),
    'CCIT Eval Submitted?': gen_name(),
    'Location': gen_location(),
    'Dispatch Info': gen_injury(), #what you get told by dispatch over radio
    'Chief Complaint': gen_injury(), #what the person first tells you
    'Call Type': gen_type(),
    'Disposition': gen_disposition(),
    'RLS': gen_bool(),
    'Air Medical': gen_air(),
    'Equipment Used': [gen_equipment() + ", ", gen_equipment() + ", ", gen_equipment()], #what equipment is used-- cravats, bandaid, glucose meter
    'Glucometer Used': gen_bool(),
    'Key Card Used': gen_bool(),
    'EtOH Related': gen_bool(),
    'Presenting Problem': gen_injury(), #what we think is wrong with patient
    'Location Code': gen_code(),
    'Demographics': gen_demographics(),
    'Rec': gen_time(),
    'EnR': gen_time(),
    'OS': gen_time(),
    'From': gen_time(),
    'Dest': gen_time(),
    'In S': gen_time(),
  }
  return call

db.calls.remove({})
for i in range(100):
  db.calls.insert(gen_example_call())
